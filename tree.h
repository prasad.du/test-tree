#ifndef _TREE_H_
#define _TREE_H_

struct TreeNode {
	int val;
	struct TreeNode *left;
	struct TreeNode *right;
};

struct TreeNode *allocateNode(int val);

void deleteNode(struct TreeNode *n);
struct TreeNode *buildTree(int *arr, int n);
void addNodeToTree(struct TreeNode *root, struct TreeNode *node);
void printTreeInOrder(struct TreeNode *root);

#endif
