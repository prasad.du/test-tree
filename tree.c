#include <stdio.h>
#include <stdlib.h>

#include "tree.h"

struct TreeNode *allocateNode(int val)
{
	struct TreeNode *n;
	n = (struct TreeNode *) malloc(sizeof(int));
	if (n) {
		n->val = val;
		n->left = NULL;
		n->right = NULL;
	}
	return n;
}

void deleteNode(struct TreeNode *n)
{
	if (n) {
		free (n);
		n = NULL;
	}
}

void addNodeToTree(struct TreeNode *root, struct TreeNode *node)
{
	if (node->val < root->val) {
		if (root->left == NULL) {
			root->left = node;
		} else {
			addNodeToTree(root->left, node);
		}
	} else if (node->val > root->val) {
		if (root->right == NULL) {
			root->right = node;
		} else {
			addNodeToTree(root->right, node);
		}
	}
	else {
		printf ("discarding duplicate `entry %d\n", node->val);
	}
	return;
}

struct TreeNode *buildTree(int *arr, int n)
{
	int i;
	struct TreeNode *node, *root;
	root = allocateNode(*(arr + 0));

	for (i = 1; i < n; i++) {
		node = allocateNode(*(arr + i));
		addNodeToTree(root, node);
	}
	return root;
}

void printTreeInOrder(struct TreeNode *root)
{
	if (root == NULL) {
		return;
	}
	printTreeInOrder(root->left);
	printf ("%-5d", root->val);
	printTreeInOrder(root->right);
}

