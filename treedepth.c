#include "tree.h"
#include <stdio.h>

int max(int a, int b)
{
	return (a > b ? a : b);
}

int depth = 0;
void treeDepth (struct TreeNode *root, int mydepth)
{
	if (root == NULL)
		return;
	if (root->left == NULL && root->right == NULL)
		depth = max(depth, mydepth);
	treeDepth(root->left, mydepth+1);
	treeDepth(root->right, mydepth+1);
}

int main (int argc, char **argv)
{
	struct TreeNode *root;

	int arr [] = {10,100, 20, 50, 30, 80, 25, 7, 3, 2};
	root = buildTree (arr, sizeof(arr) / sizeof(int));
	printTreeInOrder(root);
	printf ("\n");

	treeDepth(root, 0);
	printf ("depth of tree: %d", depth);




	return 0;
}

